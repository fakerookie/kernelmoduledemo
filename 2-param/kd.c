#include <linux/kernel.h>
#include <linux/module.h>
/* #include <linux/init.h>        Do not need this two head
   #include <linux/moduleparam.h> file in NEWEST kernel*/

static char * param;
module_param(param, charp, 0644);

static int __init kd_init(void)
{
        int i;
        for(i = 0; i < 10; i++) {
                printk(KERN_INFO "Hello, KERNEL! I am %s.\n", param);
        }
        return 0;
}

static void __exit kd_exit(void)
{
        printk(KERN_INFO "Bye, KERNEL! From %s.\n", param);
}

module_init(kd_init);
module_exit(kd_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alonzo");
MODULE_DESCRIPTION("A KERNEL DEMO WITH PARAM.");

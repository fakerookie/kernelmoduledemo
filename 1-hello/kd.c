#include <linux/kernel.h>
#include <linux/module.h>
// #include <linux/init.h> Do not need this head file in NEWEST Kernel

static int __init kd_init(void)
{
        int i;
        for (i = 0; i < 10; i++) {
                printk(KERN_INFO "Hello! Kernel!");
        }
        return 0;
}

static void __exit kd_exit(void)
{
        printk(KERN_INFO "Bye! Kernel1");
}

module_init(kd_init);
module_exit(kd_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alonzo");
MODULE_DESCRIPTION("A KERNEL MODULE DEMO");
